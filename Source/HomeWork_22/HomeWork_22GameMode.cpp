// Copyright 1998-2019 Epic Games, Inc. All Rights Reserved.

#include "HomeWork_22GameMode.h"
#include "HomeWork_22PlayerController.h"
#include "HomeWork_22Character.h"
#include "UObject/ConstructorHelpers.h"

AHomeWork_22GameMode::AHomeWork_22GameMode()
{
	// use our custom PlayerController class
	PlayerControllerClass = AHomeWork_22PlayerController::StaticClass();

	// set default pawn class to our Blueprinted character
	static ConstructorHelpers::FClassFinder<APawn> PlayerPawnBPClass(TEXT("/Game/Blueprints/Character/TopDownCharacter"));
	if (PlayerPawnBPClass.Class != NULL)
	{
		DefaultPawnClass = PlayerPawnBPClass.Class;
	}
}